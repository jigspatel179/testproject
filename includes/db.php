<?php
	// Database configuration
	$servername = "localhost";
	$username = "root";
	$password = "";
	$db_name = "db_user";

	// Create database connection
	$conn = new mysqli($servername,$username,'',$db_name);

	// Check connection
	if ($conn -> connect_errno) 
	{
	  echo "Failed to connect to MySQL: " . $conn -> connect_error;
	  exit();
	}
?>