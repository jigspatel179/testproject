<?php 
	include "includes/db.php";
	include "includes/session.php";
	include "includes/functions.php";

	$_SESSION["tracking_url"]=$_SERVER["PHP_SELF"];
	confirm_login();

	$status ="";

	if(isset($_POST["btnstatus"]) && !empty($_POST["status"])){
		$status = $_POST["status"];
		$sql = "SELECT * FROM user WHERE status='".$status."'";
	}else{
		$sql = "SELECT * FROM user";
	}
	
?>

<!DOCTYPE html>
<html>
	<head>
		<title>User List</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	</head>

	<body>
		<div style="height:10px;background:#27aae1;"></div>
		<!-- Header -->
		<header class="bg-dark text-white py-3">
		    <div class="container">
		        <div class="row">
		            <div class="col-md-12">
		            	<a href="logout.php" style="float:right;">
							<button type="button" class="btn btn-primary">logout</button>
						</a>
		                <h2 ><i class="fas fa-user text-success" style="color:#27aae1;"></i> <?php if(isset($_SESSION["Userid"])){echo $_SESSION["Username"];}?> </h2>
		                 
		        	</div>
		        </div> 
		    </div>
		</header>
		<!-- Header End-->
		<?php
         //   echo ErrorMessage();
          //  echo SuccessMessage();
            ?>
		<form action="user_list.php" method="post">	
			<div class="offset-lg-2 col-lg-8 pt-3">
				<select id="status" name="status">
					<option  value="">Select Option</option>
					<option value="active">active</option>
					<option value="deactive">deactive</option>
				</select>
				<input type="submit" name="btnstatus" value="Apply">
				<br>
				<table class="table table-striped table-hover">
					<thead>
					    <tr>
						    <th scope="col">#</th>
						    <th scope="col">Username</th>
						    <th scope="col">Phone Number</th>
						    <th scope="col">Email</th>
						    <th scope="col">User Files</th>
						    <th scope="col">Feedback</th>
						    <th scope="col">Status</th>
						    <th scope="col">Action</th>
					    </tr>
				    </thead>
					<tbody>
					  	<?php

							$result = $conn->query($sql);
							if ($result->num_rows > 0) {
							  while($row = $result->fetch_assoc()) {
						  	
						  	?>
						    <tr>
							    <th scope="row"><?php echo  $row["id"];?></th>
							    <td><?php echo  $row["username"];?></td>
							    <td><?php echo  $row["contact"];?></td>
							    <td><?php echo  $row["emial"];?></td>
							    <td><?php echo  $row["user_files"];?></td>
							    <td><?php echo  $row["feedback"];?></td>
							    <td><?php echo  $row["status"];?></td>
							    <td>
									<button type="button" class="btn btn-success edit" data-id="<?php echo $row["id"];?>" onclick="editData('<?php echo $row["id"];?>')">Edit</button></a>
							     	<button type="button" class="btn btn-danger delete" data-id="<?php echo $row["id"];?>">Active/Deactive</button>
							    </td>
						    </tr>
						    <?php
						    	}
						    }
					    ?>
					 </tbody>
				</table>
			</div>
		</form>
			
		<script>
		$(document).ready(function($){
			$('body').on('click', '.delete', function () {
		            if (confirm("Are are want to change status of user?") == true) {
		            var id = $(this).data('id');
		                // console.log("delete"+id);
		                $.ajax({
			                type:"POST",
			                url: "delete.php",
			                data: { id: id },
			                dataType: 'json',
			                success: function(res){
			                   window.location.reload();
			                },
			                error: function(jqXHR, textStatus, errorThrown){
			                    alert('error: ' + textStatus + ': ' + errorThrown);
			                }
		                });
		            }
		    });
		});

		var editData = function(id){window.open("edit.php?id="+id,"_self");};
		</script>
	</body>
</html>