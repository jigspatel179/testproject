<?php

    include "includes/db.php";
    include "includes/functions.php";
    include "includes/session.php";

    if(isset($_SESSION["Userid"])){
        redirect_to($_SESSION["tracking_url"]);
    }else{
       // redirect_to("login.php");
    }
    
    $usernameErr = $emailErr = $phonenumberErr = "";
    $username = $password = $email = $contact= $feedback ="";
    $flag=0;
    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = ''; 

    // File upload configuration 
    $allowTypes = array('jpg','png','jpeg','gif');


    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        if (empty($_POST["username"])) {
            $usernameErr = "UserName is required";
        } else {
            $username = test_input($_POST["username"]);
            
            if (!preg_match("/^[a-zA-Z-' ]*$/",$username)) {
              $usernameErr = "Only letters and white space allowed";
            }
        }

        if (empty($_POST["username"])) {
            //$nameErr = "Name is required";
        } else {
            $username = test_input($_POST["username"]);
            $sql="select * from user where username = '".$username."'";
            $res= $conn->query($sql);

            if($res->num_rows){
              $usernameErr="user already exist...";
            }else{
              $flag=1;
            } 
        }

        if (empty($_POST["userEmail"])) {
            $emailErr = "Email is required";
        } else {
            $email = test_input($_POST["userEmail"]);
            
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
              $emailErr = "Invalid email format";
            }
        }

        if (empty($_POST["userNumber"])) {
            $phonenumberErr = "Number is required";
        } else {
            $contact = test_input($_POST["userNumber"]);
           
            if (strlen($contact) < 10 || strlen($contact) >10) {
               $phonenumberErr = "Enter only 10 digit";
             } 
        }

       /* $hash="$2y$10$";$salt="iusesomthingstringof22";$password=crypt($_POST["userPass"],$hash.$salt);*/
        $password = md5($_POST["userPass"]);
        $feedback=$_POST["userFeedback"];
      
        $fileNames = array_filter($_FILES['userFiles']['name']); 
        if(!empty($fileNames)){ 
            foreach($_FILES['userFiles']['name'] as $key=>$val){ 

                // File upload path 
                $fileName = basename($_FILES['userFiles']['name'][$key]); 
                $targetFilePath = $targetDir . $fileName; 
                 
                // Check whether file type is valid 
                $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 
                if(in_array($fileType, $allowTypes)){ 
                    $insertValuesSQL .= $fileName.","; 
                }else{ 
                    $errorUploadType .= $_FILES['userFiles']['name'][$key].' | '; 
                } 
            } 
             
        } 

        if($flag){
            $insertValuesSQL = substr(trim($insertValuesSQL),0,-1); 

            $ins="INSERT INTO `user` (`id`, `username`, `password`, `contact`, `emial`, `user_files`,`feedback`) VALUES (NULL, '$username', '$password', '$contact', '$email', '$insertValuesSQL' , '$feedback')";
            
            if($conn->query($ins)){ 
       
                $dir= $conn->insert_id;
                $thisdir = "uploads";
                if(mkdir($thisdir ."/".$dir , 0777)){
                    $targetDir = "uploads/".$dir."/";
                    $fileNames = array_filter($_FILES['userFiles']['name']); 
                    if(!empty($fileNames)){ 
                        foreach($_FILES['userFiles']['name'] as $key=>$val){ 
                          
                            // File upload path 
                            $fileName = basename($_FILES['userFiles']['name'][$key]); 
                            $targetFilePath = $targetDir . $fileName; 
                             
                            // Check whether file type is valid 
                            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION); 
                            if(in_array($fileType, $allowTypes)){ 
                                // Upload file to server 
                                if(move_uploaded_file($_FILES["userFiles"]["tmp_name"][$key], $targetFilePath)){ 
                                }else{ 
                                    $errorUpload .= $_FILES['userFiles']['name'][$key].' | '; 
                                } 
                            }else{ 
                                $errorUploadType .= $_FILES['userFiles']['name'][$key].' | '; 
                            } 
                        } 
             
                    } 
                }
                else
                // echo "Failed to create directory...";
                $errorUpload = !empty($errorUpload)?'Upload Error: '.trim($errorUpload, ' | '):''; 
                $errorUploadType = !empty($errorUploadType)?'File Type Error: '.trim($errorUploadType, ' | '):''; 
                $errorMsg = !empty($errorUpload)?'<br/>'.$errorUpload.'<br/>'.$errorUploadType:'<br/>'.$errorUploadType; 
                $statusMsg = "Files are uploaded successfully.".$errorMsg; 
                redirect_to("login.php");
                }else{ 
                $statusMsg = "Sorry, there was an error uploading your file."; 
            } 
        }
    }

    // Clear input data 

    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
   }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <!-- <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  
    <title>Admin page </title>
    <style>
        .error {color: #FF0000;}
    </style>
    <script>
        function validateForm(){

            var userpassword = document.getElementById('userPass').value;
            var phonenumber = document.getElementById('userNumber').value;
            var email = document.getElementById('userEmail').value;
                
            if(userpassword.length < 6){
                document.getElementById("passErr").innerHTML = "Password must be at least 6 characters long"; 
                document.getElementById('userPass').focus();
                return false;
            }

            if(phonenumber.length > 10 || phonenumber.length < 10){
                document.getElementById("numberErr").innerHTML = "Number should be 10 digit long"; 
                document.getElementById('userNumber').focus();
                return false;
            }

            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
         
            if (reg.test(email) == false) 
            {
                document.getElementById("emailErr").innerHTML = "You have entered an invalid email address!";
                document.getElementById('userEmail').focus();
                return (false);
            }
        }
     
        function countChars(obj){
            var minLength = 6;
            var strLength = obj.value.length;
    
            if(strLength < maxLength){
                document.getElementById("passErr").textContent = "Password must be at least 6 characters long";
            }
        }
        function userExist()
        {
            var UserName = document.getElementById('username');
            if(UserName.value != "")
            {
                xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200){
                        var value = xmlhttp.responseText;
                        //console.log(value);
                        if(value.length > 1){
                            document.getElementById('userErr').innerHTML="User Name Already exist please choose Other User Name";
                            UserName.focus();
                        }else{
                            document.getElementById('userErr').innerHTML="";
                        }
                        }
                      }
                    xmlhttp.open("GET","checkUserName.php?username="+UserName.value,true);
                    xmlhttp.send();
            }
        }
        
        $('input[name="userNumber"]').keyup(function(e){
            if (/\D/g.test(this.value)){
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
        });
    </script>
</head>
<body>
   

    <!-- Main-->
    <section class="container py-2 mb-4">
        <div class="row" >
            <div class="offset-lg-3 col-lg-6" style="height: 800px;">
                <form action="user.php" method="post" onsubmit="return validateForm()" enctype="multipart/form-data">
                    <div class="card bg-secondary text-light">
                        <div class="card-header">
                            <h2>Add New User</h2>
                        </div>
                        <div class="card-body bg-dark">
                            <div class="form-group">
                                <label for="Username">Username</label>
                                <input class="form-control" name="username" type="text" id="username" onblur="userExist()" required>
                                <span class="error" id="userErr"></span>
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input class="form-control" name="userPass" type="password" id="userPass" onkeyup="countChars(this);" required>
                                 <span class="error" id="passErr"></span>
                            </div>
                            <div class="form-group">
                                <label for="PhoneNumber">Phone Number</label>
                                <input class="form-control" name="userNumber" type="tel" id="userNumber" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  required >
                                <span class="error" id="numberErr"></span>
                            </div>
                            <div class="form-group">
                                <label for="Email">Email Id </label>
                                <input class="form-control" name="userEmail" type="emial" id="userEmail"  required> 
                                <span class="error" id="emailErr"></span>
                            </div>
                            <div class="form-group">
                                <label for="Email"> Select Image Files to Upload</label>
                                <input class="form-control" name="userFiles[]" type="file" id=""  multiple required> 
                                <span class="error" id="fileErr"><?php echo $statusMsg;?></span>
                            </div>
                            <div class="form-group">
                                <label for="Feedback">Feedback</label>
                                <textarea id="userFeedback" name="userFeedback" class="form-control"></textarea>
                            </div>
                            <div class="col-lg-6 offset-lg-3 mb-1 pt-2">
                                <button type="submit" name="submit" class="form-control btn btn-success btn-block">
                                <i class="fas fa-check"></i> submit
                                </button>
                            </div>           
                        </div>
                    </div>
                </form>               
            </div>
        </div>
    </section>
    <!-- Main End-->
</body>
</html>