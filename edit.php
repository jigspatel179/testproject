<?php
	include "includes/db.php";
	include "includes/session.php";
	include "includes/functions.php";

	$edit_id = $_GET['id'];

	if(isset($_GET['id'])){
		$sel="SELECT * FROM user WHERE id=".$_GET['id'];
		$result=$conn->query($sel);

		while($row = $result->fetch_assoc()){
			$username=$row["username"];
			$phonenumber=$row["contact"];
			$email=$row["emial"];
			$userfiles=$row["user_files"];
			if(empty($userfiles)){
				$userfiles="";
			}else{
				$userfiles=explode(",",$row["user_files"]);
			}
			$existingfile=$row["user_files"];
			//array_pop($userfiles);
			//print_r($userfiles);
		}
	}

	if(isset($_POST["path"])){
		unlink($_POST["path"]);
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
	    <meta name="viewport"
	    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	    
	    <!-- <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">-->
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	   
	    <!-- jQuery -->
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	   	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	   		<title></title>
			<style type="text/css">
				.error {color: #FF0000;}
				.image-area {
					display: block;
				  position: relative;
				  width:15%;
				  background: #333;
				  float :left;
				  margin-right: 13px;
				  margin-bottom: 10px;
				}
				.image-area img{
				
				  height: auto;
				}
				.remove-image {
				display: none ;
				position: absolute;
				top: -10px;
				right: -10px;
				border-radius: 5em;
				padding: 2px 6px 3px;
				text-decoration: none;
				font: 700 21px/20px sans-serif;
				background: #555;
				border: 3px solid #fff;
				color: #FFF;
				box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
				  text-shadow: 0 1px 2px rgba(0,0,0,0.5);
				  -webkit-transition: background 0.5s;
				  transition: background 0.5s;
				}
				.remove-image:hover {
				 background: #E54E4E;
				  padding: 3px 7px 5px;
				  top: -11px;
				right: -11px;
				}
				.remove-image:active {
				 background: #E54E4E;
				  top: -10px;
				right: -11px;
				}       		
			</style>
	</head>
	<body>
		 <section class="container py-2 mb-4">
        <div class="row" >
            <div class="offset-lg-3 col-lg-6" style="height: 800px;">
                <form action="javascript:void(0)" method="post" enctype="multipart/form-data" id="">
                    <div class="card bg-secondary text-light">
                        <div class="card-header">
                            <h2>Edit Existing User</h2>
                        </div>
                        <div class="card-body bg-dark">
                        	<input type="hidden" name="editid" value='<?php echo $_GET['id']; ?>'>
                            <div class="form-group">
                                <label for="Username">Username</label>
                                <input class="form-control" name="username" type="text" id="username" required value="<?php if(isset($_GET['id'])) echo $username;?>" readonly>
                                <span class="error" id="userErr"></span>
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input class="form-control" name="userPass" type="password" id="userPass">
                                 <span class="error" id="passErr"></span>
                            </div>
                            <div class="form-group">
                                <label for="PhoneNumber">Phone Number</label>
                                <input class="form-control" name="userNumber" type="tel" id="userNumber" maxlength="10" minlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" value="<?php if(isset($_GET['id'])) echo $phonenumber;?>" required >
                                <span class="error" id="numberErr" name="numberErr"></span>
                            </div>
                            <div class="form-group">
                                <label for="Email">Email Id </label>
                                <input class="form-control" name="userEmail" type="emial" id="userEmail"  value="<?php if(isset($_GET['id'])) echo $email;?>" required> 
                                <span class="error" id="emailErr"></span>
                            </div>
                            <div class="form-group">
                            	<br>
                            		<?php
                          
                            			if(isset($_GET['id'])){
                            				if(!empty($userfiles)){
	                            			foreach ($userfiles as $key => $value) {
	                            				$image = $value;
	                            				
	                            				echo '<div class="image-area" id="image-areaa'.$key.'">
	                            					<input type="hidden" name="existingimage[]" value='.$image.'>
	                            					<img class="img-thumbnail" src="uploads/'.$edit_id.'/'.$image.'">
	                            					<button class="remove-image" id="removeBtn" style="display: inline;" data-id='.$key.' onclick="removeImage('.$key.','.$edit_id.',\''.$image.'\')"> X
	                            					</button>
	                            					</div>';
	                            			}
	                            			}
	                            		}
                                	?>
                                	<!--<input type="hidden" name="existingFile" id="existingFile" value='<?php echo $existingfile;?>'>-->
                            </div>
                           
                            <div class="form-group">
                                <!--<label for="Email" style="float:right"> Select Image Files to Upload</label>-->
                                <input class="form-control" name="userFiles[]" type="file" id="files" multiple> 
                                <span class="error" id="fileErr"></span><br>
                            </div>
                            <div class="col-lg-6 offset-lg-3 mb-1 pt-2">
                                <button type="submit" id="btnEdit" name="submit" class="form-control btn btn-success btn-block" data-id="<?php echo $edit_id;?>">
                                <i class="fas fa-check"></i> Edit
                                </button>
                            </div>           
                        </div>
                    </div>
                </form>               
            </div>
        </div>
    </section>
    <script type="text/javascript">
		
		$('input[name="userNumber"]').keyup(function(e){
            if (/\D/g.test(this.value)){
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
        });

    	function removeImage(id,edit_id,imagename){
	   		//console.log(edit_id + " => "+ id +" "+imagename);
    		document.getElementById('image-areaa'+id).remove();
    		    var file_path = 'uploads/'+edit_id+'/'+imagename;
	            console.log(file_path);
	            $.ajax({
			                type:"POST",
			                url: "auto_delete_image.php",
			                data: {path:file_path,edit_id:edit_id,image:imagename},
			              //  dataType: 'json',
			                success: function(res){
			                  	 //window.location.reload();
			                },
			                error: function(jqXHR, textStatus, errorThrown){
			                    alert('error: ' + textStatus + ': ' + errorThrown);
			                }
		                });	 
	        
    	}

		$(document).ready(function() {
			if (window.File && window.FileList && window.FileReader) {
				$("#files").on("change", function(e) {
				  var files = e.target.files,
				  filesLength = files.length;
				  
				  for (var i = 0; i < filesLength; i++) {
				    var f = files[i];
				    var fileReader = new FileReader();
				    fileReader.onload = (function(e) {
					    var file = e.target;

					    $("<div class=\"image-area\">" +
					        "<input type=\"hidden\" name=\"newuploadedimage[]\" value=\""+f.name+"\"><img class=\"img-thumbnail\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
					        "<span class=\"remove-image\" style=\"display: inline;\">&#215;</span>" +
					        "</div>").insertBefore("#files");
					    $(".remove-image").click(function(){
					        $(this).parent(".image-area").remove();
					    });				     		      
				    });
				    fileReader.readAsDataURL(f);
				  }
				 // console.log(files);
				});
			} else {
			alert("Your browser doesn't support to File API")
			}
		});
		
		$(document).on('click','#btnEdit',function(e){
	       
	        e.preventDefault();
        	var id= $("input[name='editid']").val();               
		    var password= $("input[name='userPass']").val();
		    var emailAddress= $("input[name='userEmail']").val();
		    var contact= $("input[name='userNumber']").val();
		    var existingfile= $("input[name='existingimage[]']").map(function(){return $(this).val();}).get();
		    
 		    // Field Validation

		    if(contact == "" || contact.length <10 || contact == 0)
		    {
		   		$("#numberErr").text("Number should be 10 digit long");
		   		 $("input:tel").focus();
		    }

		    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
         
            if (reg.test(emailAddress) == false) 
            {
                $("#emailErr").text("You have entered an invalid email address!");
              	$("input:email").focus();
            }

		    var form_data = new FormData();

		   // Read selected files
		    var totalfiles = document.getElementById('files').files.length;
		    for (var index = 0; index < totalfiles; index++) {
		      form_data.append("files[]", document.getElementById('files').files[index]);
		    }

		    form_data.append("updateId",id);
		    form_data.append("password",password);
		    form_data.append("contact",contact);
		    form_data.append("useremail",emailAddress);
		    form_data.append("existingfile",existingfile.join());
  
		    $.ajax({
			    method:"POST",
			    url: "update_user.php",
			    data:form_data,
			    dataType: 'json',
			    contentType: false,
    			processData: false,
     			success: function(data){
     				window.open("user_list.php","_self");
			    }
		    });
		});

    </script>
	</body>
</html>