<?php 
	require_once "includes/session.php";
	require_once "includes/functions.php";
	
	$_SESSION["Userid"]=null;
	$_SESSION["Username"]=null;
	session_destroy();
	redirect_to("login.php");
?>
