<?php
    require_once "includes/db.php";
    require_once "includes/session.php";
    require_once "includes/functions.php";

    if(isset($_SESSION["Userid"])){
        redirect_to("user_list.php");
    }

    if(isset($_POST["submit"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];
               
        if (empty($username) || empty($password)) {
            $_SESSION["Error"] = "All Field Must be filled out";
            redirect_to("login.php");
        }else {
            $found_account=login_attempt($username,$password);
            if($found_account){
                $_SESSION["Userid"]=$found_account["id"];
                $_SESSION["Username"]=$found_account["username"];
                $_SESSION["Success"]="Welcome ".$_SESSION["Username"];
            if(isset($_SESSION["tracking_url"])){
                    redirect_to($_SESSION["tracking_url"]);
                }else {
                    redirect_to("user_list.php");
                }
            }else{
                $_SESSION["Error"]="Incorrect Username/Password";
                redirect_to("login.php");
            }
        }
    }
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
       <!-- <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
      
        <title>Document</title>
    </head>
    <body>

    <!-- main area -->
    <section class="container py-2 mb-4">
        <div class="row">
            <div class="offset-sm-3 col-sm-6" style="min-height: 400px;background: "";"><br><br><br>
            <?php
                echo ErrorMessage();
                echo SuccessMessage();
            ?>
                <div class="card bg-secondary text-light pt-1">
                    <div class="card-header">
                        <h4>Welcome back</h4>
                    </div>
                        <div class="card-body bg-dark">
                        <form class="" action="login.php" method="post">
                            <div class="form-group">
                                <label for="username" class=""><span class="FieldInfo">Username</span></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class=""><span class="FieldInfo">Password</span></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <input class="form-control btn btn-primary btn-block mb-2" name="submit" type="submit" value="Sign In">
                             <a href="user.php"><input class="form-control btn btn-primary btn-block" name="submit" type="button" value="Register"></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Main area End -->
    </body>
</html>