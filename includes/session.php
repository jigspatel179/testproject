<?php
session_start();
function ErrorMessage(){
    if(isset($_SESSION["Error"])){
            $output="<div class=\"alert alert-danger\">";
            $output .= htmlentities($_SESSION["Error"]);
            $output .= "</div>";
            $_SESSION["Error"]=null;
            return $output;
    }
}

function SuccessMessage(){
    if(isset($_SESSION["Success"])){
        $output="<div class=\"alert alert-success\">";
        $output .= htmlentities($_SESSION["Success"]);
        $output .= "</div>";
        $_SESSION["Success"]=null;
        return $output;
    }
}
?>